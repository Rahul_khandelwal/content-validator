/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rahul.textmoderator.api;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import spark.Request;
import spark.Response;

/**
 *
 * @author Rahul
 */
public enum TextModerator {
    
    INSTANCE;
    
    private final Pattern BAD_WORDS_REGEX;

    private TextModerator() {
        this.BAD_WORDS_REGEX = Pattern.compile(".*(KILL|LOOT|WAR).*", Pattern.CASE_INSENSITIVE);
    }
    
    public String getInfo(Response response) {
        response.type("application/json");
        response.status(501);
        
        JSONObject responseBuilder = new JSONObject();
        responseBuilder.put("message", "Get is not supported, use POST");
        responseBuilder.put("params_required", "id and text");
        
        return responseBuilder.toJSONString();
    }
    
    public String moderate(Request request, Response response) {
        response.type("application/json");
        
        String id = request.queryParams("id");
        String text = request.queryParams("text");
        
        if (id == null || id.trim().isEmpty() || text == null || text.trim().isEmpty()) {
            response.status(400);
            JSONObject responseBuilder = new JSONObject();
            responseBuilder.put("message", "Invalid parameters");
            return responseBuilder.toJSONString();
        }
        
        JSONObject responseBuilder = new JSONObject();
        
        String badWords = this.validate(text);
        
        if (badWords == null || badWords.trim().isEmpty()) {
            responseBuilder.put("isValid", "true");
            responseBuilder.put("feedback", "Ok");
        } else {
            responseBuilder.put("isValid", "false");
            responseBuilder.put("feedback", badWords);
        }
        
        return responseBuilder.toJSONString();
    }
    
    private String validate(String text) {
        Matcher matcher = BAD_WORDS_REGEX.matcher(text);
        
        // NOTE that logic here needs to be corrected
        if (matcher.find()) {
            JSONArray array = new JSONArray();
            array.add(matcher.group(1));
            
            while(matcher.find()) {
                array.add(matcher.group(1));
            }
            
            return array.toJSONString();
        }
        
        return null;
    }
}
