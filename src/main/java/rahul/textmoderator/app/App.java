/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rahul.textmoderator.app;

import org.json.simple.JSONObject;
import rahul.textmoderator.api.TextModerator;
import spark.Spark;

/**
 *
 * @author Rahul
 */
public class App {
    
    private static final String ERROR_404_MESSAGE;
    
    static {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("message", "This page does not exists");
        jsonObject.put("required_url", "/comment/moderate");
        jsonObject.put("required_method", "get/post");
        
        ERROR_404_MESSAGE = jsonObject.toJSONString();
    }

    public static void main(String[] args) {

        // Add get call for API, which provides info
        Spark.get("/comment/moderate", (request, response) -> TextModerator.INSTANCE.getInfo(response));

        Spark.post("/comment/moderate", (request, response) -> TextModerator.INSTANCE.moderate(request, response));

        Spark.notFound((request, response) -> {
            response.type("application/json");
            return ERROR_404_MESSAGE;
        });
        
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                System.out.println("Stopping jetty server");
                Spark.stop();
            }
        });
    }

}
